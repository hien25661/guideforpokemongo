package tutorialpokemongo.miniclues.com.pokemongotutorial;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.Pokemongoguide.guideforpokemontrainer.tipsforpokemongo.R;

/**
 * Created by NewBie on 7/21/16.
 */
public class ChildActivity1 extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.child_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        WebView wv;
        wv = (WebView) findViewById(R.id.webview);
        wv.setWebViewClient(new myWebViewClient());
        String url = getIntent().getStringExtra("url");
        wv.loadUrl(url);

    }

    class myWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Intent t = new Intent(ChildActivity1.this, ChildActivity2.class);
            t.putExtra("url",url);
            startActivity(t);

            return true;
        }
    }
}
