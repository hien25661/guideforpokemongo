package tutorialpokemongo.miniclues.com.pokemongotutorial;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.Pokemongoguide.guideforpokemontrainer.tipsforpokemongo.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

public class MainActivity extends AppCompatActivity {
    AdView adView;
    InterstitialAd interstitial;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        adView = (AdView)findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        // Prepare the Interstitial Ad
        interstitial = new InterstitialAd(this);
        // Insert the Ad Unit ID
        interstitial.setAdUnitId(getResources().getString(R.string.ads_id_interstis));
        // Request for Ads
        adRequest = new AdRequest.Builder().build();
        // Load ads into Interstitial Ads
        interstitial.loadAd(adRequest);
        interstitial.setAdListener(new AdListener() {
            // Listen for when user closes ad
            public void onAdClosed() {
            }
        });
    }

    public void openApp(View v) {
        startActivity(new Intent(this, ListActivity.class));
    }

    public void shareApp(View v) {
        final String appPackageName = getPackageName(); // getPackageName()
        // from
        // Context
        // or
        // Activity
        // object
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,
                "I found awesome app!!!\nhttps://play.google.com/store/apps/details?id=" + appPackageName);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    public void openApp1(View v) {
        final String appPackageName = "media.musicplayer.songs.mp3player.audio"; // getPackageName()
        // from
        // Context
        // or
        // Activity
        // object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }

    }

    public void openApp2(View v) {
        final String appPackageName = "com.appgalaxy.pedometer"; // getPackageName()
        // from
        // Context
        // or
        // Activity
        // object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    public void openApp3(View v) {
        final String appPackageName = "com.miniclueswebbrowser.fastestbrowser.speedybrowser.quickbrowser"; // getPackageName()
        // from
        // Context
        // or
        // Activity
        // object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    public void openGame1(View v) {
        final String appPackageName = "com.minicluesgames.impossiblerun"; // getPackageName()
        // from
        // Context
        // or
        // Activity
        // object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    public void openGame2(View v) {
        final String appPackageName = "com.minicluesgames.amazingjungle"; // getPackageName()
        // from
        // Context
        // or
        // Activity
        // object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    public void openGame3(View v) {
        final String appPackageName = "com.minicluesgames.bounceball.graphball"; // getPackageName()
        // from
        // Context
        // or
        // Activity
        // object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this,AdActivity.class));
        finish();
    }
}
