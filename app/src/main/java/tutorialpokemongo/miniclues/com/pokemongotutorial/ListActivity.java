package tutorialpokemongo.miniclues.com.pokemongotutorial;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.Pokemongoguide.guideforpokemontrainer.tipsforpokemongo.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

/**
 * Created by NewBie on 7/21/16.
 */
public class ListActivity extends AppCompatActivity {
    AdView adView;
    InterstitialAd interstitial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_activity);
        //Your toolbar is now an action bar and you can use it like you always do, for example:
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        WebView wv;
        wv = (WebView) findViewById(R.id.webview);
        wv.setWebViewClient(new myWebViewClient());
        wv.loadUrl("file:///android_asset/Main/menu.html");
        adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        // Prepare the Interstitial Ad
        interstitial = new InterstitialAd(this);
        // Insert the Ad Unit ID
        interstitial.setAdUnitId(getResources().getString(R.string.ads_id_interstis));
        // Request for Ads
        adRequest = new AdRequest.Builder().build();
        // Load ads into Interstitial Ads
        interstitial.loadAd(adRequest);
        interstitial.setAdListener(new AdListener() {
            // Listen for when user closes ad
            public void onAdClosed() {
                requestNewInterstitial();
            }
        });

    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .build();

        interstitial.loadAd(adRequest);
    }

    class myWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Intent t = new Intent(ListActivity.this, ChildActivity2.class);
            t.putExtra("url", url);
            startActivity(t);
            return true;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private int countTime = 0;

    @Override
    protected void onResume() {
        super.onResume();
        boolean gotoAds = false;
        boolean isAdshow = false;
        if (countTime % 3 == 0) {
            gotoAds = true;
            if (interstitial.isLoaded()) {
                interstitial.show();
                isAdshow = true;
            }
        }
        if (gotoAds) {
            if (isAdshow) {
                countTime++;
            }
        } else {
            countTime++;
        }
    }
}
